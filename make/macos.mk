# macosx.mk
#
# Goals:
#   Configure an environment that will allow Afloat's code to be built on a macOS
#   system. The environment will support the current versions of python installed
#   either via brew or system default

##############################
# Check for and find Python 2
##############################

ifneq (, $(shell which /usr/local/bin/python))
  # Homebrew python present. Use it.
  PYTHON_ROOT:=/usr/local/bin/
else
  # Homebrew python not present. Use the system python command.
  PYTHON_ROOT:=/usr/bin/
endif

##############################
# Check for and find Python 2
##############################

include $(ROOT_DIR)/make/python.mk
