# python.mk
#
# Goals:
#   Configure an environment that will support the current versions of python
#   installed

##############################
# Check for and find Python 2
##############################

# Get Python version (e.g. Python 2.7.17), remove the period(.) to separate into
# major/minor/patch (e.g. Python 2 7 17), then put 2nd, 3rd, and 4th values into
# a wordlist.
PYTHON_VERSION_=$(wordlist 2,4,$(subst ., ,$(shell $(PYTHON_ROOT)python -V 2>&1)))

# Get major version from aforementioned wordlist
PYTHON_MAJOR_VERSION_=$(word 1,$(PYTHON_VERSION_))

# Just in case Make has some weird scope stuff
PYTHON=0

# If the major Python version is the one we want..
ifeq ($(PYTHON_MAJOR_VERSION_),2)
	# Then we can just use the normal Python executable
	PYTHON:=$(PYTHON_ROOT)python
else
	# However, this isn't always the case..
	# Let's look for `python2`. If `which` doesn't return a null value, then it exists!
	ifneq ($(shell which $(PYTHON_ROOT)python2), "")
		PYTHON:=$(PYTHON_ROOT)python2
	else
		# And if it doesn't exist, let's use the default Python, and warn the user.
		# PYTHON2 NOT FOUND.
		PYTHON:=$(PYTHON_ROOT)python
		echo "Python2 not found."
	endif
endif

export PYTHON
