#ifndef PAYLOAD_DATA_SPEC_H
#define PAYLOAD_DATA_SPEC_H

enum PUMP_EVENT {
   PUMP_EVENT_OFF = -1,
   PUMP_EVENT_NONE = 0,
   PUMP_EVENT_ON = 1,
};


/*
 * time_t Timestamp
 * uint32_t measurementCount
 * uint16_t pumpAdc_count
 * enum isPumpToggleEvent
 * enum didPacketTriggerPublish
 */
typedef std::tuple<int64_t, uint32_t, uint16_t, enum PUMP_EVENT, bool> payload_t;


#endif  // PAYLOAD_DATA_SPEC_H
