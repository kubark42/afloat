#ifndef HOLDING_TANK_H
#define HOLDING_TANK_H

#include "Libraries/AfloatAbstractNode.h"

class BilgeSensor : public AfloatAbstractNode {
public:
   BilgeSensor(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode>, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks);

   void identityHandler(const char *event, const char *jsonMsgPacket);
   void publishData(payload_t payload, uint32_t numberOfRetries = 0);
   void republishData(const payload_t &payloadData, uint32_t numberOfRetries = 0);
   void setupPremesh();
   void setupPostmesh();
   enum SLEEP_STATE performMeasurement(enum SLEEP_STATE sleepState);

private:
   void bilgePumpCycleInterrupt();

   int setTriggerTimeout(String command);
   int setTransmissionTimeout(String command);

   bool isDataCollectionComplete = true;
   bool isBilgePumpPowered = false;
   enum PUMP_EVENT pumpEvent = PUMP_EVENT_NONE;  // Indicates if the bilge pump has triggered

   uint32_t measurementCount = 0;
   int64_t pumpTurnOnTime_utc_ms = 0;
   int64_t pumpTurnOffTime_utc_ms = 0;
   int64_t lastPublishTime_utc_ms = 0;

   uint32_t pumpMinimumDetectionTimeout_ms;
   uint32_t depowerTransmissionTimeout_ms;
};



#endif  // HOLDING_TANK_H
