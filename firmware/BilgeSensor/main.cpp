/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"
#include "Libraries/utilities.h"

#include "gitcommit.h"
#include "node_configuration.h"

#if 0
// Configure debug output. Makes lots of console spew, but helps diagnose Mesh/Cloud connection failures
SerialDebugOutput debugOutput;
#endif


//! Local defines
#define CONNECTION_INTERVAL_S 30
#define MAX_NUM_PENDING_ACKS 10

#define WATCHDOG_TIMEOUT_MS (60*1000)


//! Local variables
ApplicationWatchdog softwareWatchDog(WATCHDOG_TIMEOUT_MS, System.reset); // reset the system after 60 seconds if the application is unresponsive

NODE_TYPE node(String(SENSOR_NAME),
                        String(DATA_FIELD_NAME),
                        String(DESCRIPTION),
                        WAKE_PINS,
                        WAKE_INTERRUPT_MODES,
                        CONNECTION_INTERVAL_S,
                        MAX_NUM_PENDING_ACKS);

//! Local functions


/**
 * @brief setup Arduino setup routine. Runs once before launching into main().
 */
void setup()
{
   Serial.println("Booting node...");

   // Perform any node startup routines here. This is for setup which should occur before the mesh connection is established.
   node.setupPremesh();

   // Subscribe to all `whoami`. Use a strange call to get around Arduino's documented issues with attaching
   // a callback to a class method. C.f. https://community.particle.io/t/c-member-functions-as-function-pointers/21574
   Mesh.subscribe(SENSOR_NAME, [](const char *event, const char *byteArrayPacket){node.identityHandler(event, byteArrayPacket);});

   // Publish vitals every 60 seconds, indefinitely
   Particle.publishVitals(60);

   // Block until the time is valid
   while (Time.isValid() == false) {
      // Blocking wait. This isn't necessarily the smart thing to do, as the data still
      // has validity even if the time isn't set. However, it will take a fair amount of
      // care to ensure that the system handles an invalid time correctly, and so until
      // that is architected the safest thing to do is block.
   }

   // Perform any node startup routines here. This is for setup which should occur after the mesh connection is established.
   node.setupPostmesh();

   /* DEBUG output to indicate setup completed */
   Serial.println("Node " + String(SENSOR_NAME) + " booted. Entering into main loop...");
}



/**
 * @brief loop Inifinite loop. FreeRTOS equivalent to while(1){}.
 */
void loop()
{
   bool shouldDelaySleep = false;
   bool shouldSleepImmediately = false;

   // Initialize defaults for sleep state output
   static enum SLEEP_STATE sleepState = SLEEP_DONT_SLEEP;

   // Feed the watchdog
   softwareWatchDog.checkin();

   // Run preliminary loop code
   node.beginLoop(&sleepState, &shouldSleepImmediately);

   // Check if we should jump immediately to sleep
   if (shouldSleepImmediately == true) {
      Serial.println("Going to sleep immediately...");

      goto sleep;
   }


   //================================
   // HANDLE SENSOR EVENTS
   //================================

   sleepState = node.performMeasurement(sleepState);

   //================================
   // DO GENERIC HOUSEKEEPING
   //================================

   // Check the battery
   node.updateBatteryState();

   // Do network housekeeping
   node.performNetworkHousekeeping(&sleepState, &shouldDelaySleep);

   // Check if we should delay sleeping
   if (shouldDelaySleep == true) {
      // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
      Serial.println("[MESH] Delaying sleep...");
      delay_nonblocking(1000);
   } else {
sleep:  // GOTO jump point
      switch  (sleepState) {
      case SLEEP_TILL_NEXT_SAMPLE:
      case SLEEP_TILL_INTERRUPTED:
      case SLEEP_TILL_SYNC_TIME:
         node.handleSleep(sleepState);
         break;
      case SLEEP_UNDETERMINED:
      case SLEEP_DONT_SLEEP:
         // 1000ms delay to make sure the loop doesn't run too fast. Too many publishes per second overwhelms things
         delay_nonblocking(1000);
         break;
      }
   }
}
