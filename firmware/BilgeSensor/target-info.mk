# Add this board to the list of buildable boards
ALL_BOARDS += bilgesensor

# Set the board here that matches your Particle
# Should be one of: xenon,argon,boron
bilgesensor_platform := xenon

bilgesensor_serial_number := e00fce68850a9d213b5d525b
