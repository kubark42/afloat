#ifndef NODECONFIGURATION_H
#define NODECONFIGURATION_H

#include "BilgeSensor.h"

#define NODE_TYPE BilgeSensor

#define SENSOR_NAME "bilge_sensor"
#define DATA_FIELD_NAME "cycles"
#define DESCRIPTION "Monitors the bilge pump"

#define BILGE_SENSOR_PIN D8
#define BILGE_SENSOR_INTERRUPT_MODE CHANGE

#define PUMP_VOLTAGE_SENSOR_PIN A0

#define WAKE_PINS {BILGE_SENSOR_PIN}
#define WAKE_INTERRUPT_MODES {BILGE_SENSOR_INTERRUPT_MODE}


#endif  // NODECONFIGURATION_H
