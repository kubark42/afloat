/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "Libraries/mesh_ack.h"

#include "gitcommit.h"
#include "node_configuration.h"
#include "Libraries/utilities.h"
#include "Libraries/third_party/CircularBuffer/CircularBuffer.h"

/**
 * @brief bilgePumpCycleInterrupt Bilge pump interrupt service routine
 */
void BilgeSensor::bilgePumpCycleInterrupt()
{
   // Capture the pin state
   isBilgePumpPowered = (digitalRead(BILGE_SENSOR_PIN) == HIGH ? true : false);

   // Get timestamp
   uint64_t pumpStateChangeTime_ms = getUtc_ms();

   // Depending on whether the bilge is powered or depowered, set some relevant variables.
   if (isBilgePumpPowered == true) {
      pumpTurnOnTime_utc_ms = pumpStateChangeTime_ms;
      pumpEvent = PUMP_EVENT_ON;

      isDataCollectionComplete = false;
   } else {
      pumpTurnOffTime_utc_ms = pumpStateChangeTime_ms;
      pumpEvent = PUMP_EVENT_OFF;
   }
}


/**
 * @brief BilgeSensor::performMeasurement
 */
enum SLEEP_STATE BilgeSensor::performMeasurement(enum SLEEP_STATE sleepState)
{
   static CircularBuffer<payload_t,2000> payloadBuffer;
   bool shouldPublishData = false;  // By default, don't publish data

   // Get UTC timestamp, in [ms]
   uint64_t utc_ms = getUtc_ms();

   // Check if the pump is off and the data collection measurement is complete.
   if (isBilgePumpPowered==false && isDataCollectionComplete==true) {
      // Check if there is queued up data which is getting stale in the buffer. I figure an hour is already a long time to be
      // sittin on the data...
      if (!payloadBuffer.isEmpty() && (utc_ms - pumpTurnOffTime_utc_ms) > 3600*1000) {
         Serial.println("Publishing " + String(payloadBuffer.size()) + " packets of stale data");

         // Loop until the buffer is empty
         while (!payloadBuffer.isEmpty()) {
            // Publish the tuple
            publishData(payloadBuffer.shift());
         }
      } else {
      }

      return sleepState;
   }

   // Measure voltage
   uint16_t pumpVoltageMeasurement_cnt = analogRead(PUMP_VOLTAGE_SENSOR_PIN);

   // Determine if we should publish, go to sleep, etc...
   if (isBilgePumpPowered == true) {
      // Check if the Node has published since it last woke up.
      if (lastPublishTime_utc_ms < pumpTurnOnTime_utc_ms) {
         // Should we publish data?
         if (utc_ms - pumpTurnOnTime_utc_ms > pumpMinimumDetectionTimeout_ms) {
            shouldPublishData = true;
         }
      } else {
         // Should we publish data?
         if (utc_ms - lastPublishTime_utc_ms > pumpMinimumDetectionTimeout_ms) {
            shouldPublishData = true;
         }
      }

      // Sleep until the next sample
      /*sleepState = SLEEP_TILL_NEXT_SAMPLE;*/
   } else {
      // Run the sensing for a few seconds after the pump depowers
      if (utc_ms - pumpTurnOffTime_utc_ms > depowerTransmissionTimeout_ms) {
         // Should we publish data?
         if (pumpTurnOffTime_utc_ms - pumpTurnOnTime_utc_ms > pumpMinimumDetectionTimeout_ms) {
            shouldPublishData = true;
         }

         isDataCollectionComplete = true;

         // Go back to sleep until the next interrupt
         Serial.println("Pump is off, sleeping now.");
         sleepState = SLEEP_TILL_INTERRUPTED;
      }
   }

   // Increment measurement count
   measurementCount++;

   // Add measurements to accumulator
   payloadBuffer.push(payload_t(utc_ms, measurementCount, pumpVoltageMeasurement_cnt, pumpEvent, shouldPublishData));

   if (shouldPublishData == true) {
      // Iterate over the old data in the buffer until it's empty
      while (!payloadBuffer.isEmpty()) {
         // Publish the tuple
         publishData(payloadBuffer.shift());
      }

      // Update the last published time
      lastPublishTime_utc_ms = utc_ms;
   }

   // Reset the the pump event flag
   if (pumpEvent != PUMP_EVENT_NONE) {
      pumpEvent = PUMP_EVENT_NONE;
   }

   return sleepState;
}


BilgeSensor::BilgeSensor(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode> edgeTriggerModes, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks) :
   AfloatAbstractNode(sensorName, dataFieldName, description, wakeUpPins, edgeTriggerModes, connectionInterval_s, maxNumPendingAcks)
{
   pumpMinimumDetectionTimeout_ms = 8*1000;
   depowerTransmissionTimeout_ms = 5*1000;
}


void BilgeSensor::identityHandler(const char *event, const char *jsonMsgPacket)
{
   Serial.printlnf("[MESH][RX] event=%s, payload=%s", event, jsonMsgPacket ? jsonMsgPacket : "NULL");

   // Perform generic message processing
   bool ret = identityMsgProcessor(event, jsonMsgPacket);

   /* If the generic processor couldn't handle the message, then maybe it requires specialized
    * handling by the specific node.
    */
   if (ret == false) {
      /*
       * Perform any extra message handling here.
       * .
       * .
       * .
       */
   }
}


int BilgeSensor::setTriggerTimeout(String command)
{
   // Attempt to convert the command into an integer
   pumpMinimumDetectionTimeout_ms = command.toInt();

   // Annoyingly, there is no way to detect conversion failure in Arduino's
   // `toInt()` method. It simply returns a 0 on failure. However, we can
   // infer that the 0 was an intentional 0 if the `command` was also the
   // string "0".
   // NOTE: a successful strcmp() returns 0, so the following fails if both
   // the converted value is 0 AND the string command is non-"0".
   if (pumpMinimumDetectionTimeout_ms == 0 && strcmp(command, "0")) {
      return -1;
   }

   // Check that the value is within spec. Currently, we allow all non-negative numbers.
   if (pumpMinimumDetectionTimeout_ms < 0) {
      return -1;
   }

   // Return the sent value as a way of confirming success
   return pumpMinimumDetectionTimeout_ms;
}


int BilgeSensor::setTransmissionTimeout(String command)
{
   // Attempt to convert the command into an integer
   depowerTransmissionTimeout_ms = command.toInt();

   // Annoyingly, there is no way to detect conversion failure in Arduino's
   // `toInt()` method. It simply returns a 0 on failure. However, we can
   // infer that the 0 was an intentional 0 if the `command` was also the
   // string "0".
   // NOTE: a successful strcmp() returns 0, so the following fails if both
   // the converted value is 0 AND the string command is non-"0".
   if (depowerTransmissionTimeout_ms == 0 && strcmp(command, "0")) {
      return -1;
   }

   // Check that the value is within spec. Currently, we allow all non-negative numbers.
   if (depowerTransmissionTimeout_ms < 0) {
      return -1;
   }

   // Return the sent value as a way of confirming success
   return depowerTransmissionTimeout_ms;
}


/**
 * @brief BilgeSensor::publishData
 * @param payloadData
 * @param numberOfRetries
 */
void BilgeSensor::publishData(payload_t payloadData, uint32_t numberOfRetries)
{
   static uint32_t packetCount = 1;

   // Helper variables
   int64_t measurementTime_tmp;
   uint32_t measurementCount_tmp;
   uint16_t pumpAdcCount_tmp;
   enum PUMP_EVENT pumpToggleEvent_tmp;
   bool didTriggerPublish_tmp;

   // Fetch the tuple and expand it
   std::tie(measurementTime_tmp, measurementCount_tmp, pumpAdcCount_tmp, pumpToggleEvent_tmp, didTriggerPublish_tmp) = payloadData;


   /* Prepare the JSON object. Please note that the JSON can have any number
    * of data elements and types in it. For different Afloat nodes, expand it
    * as appropriate.
    */
   const size_t capacity = JSON_OBJECT_SIZE(6);
   DynamicJsonDocument sensorPacket(capacity);  // TODO: Change to static JSON document type
   sensorPacket["time"] = measurementTime_tmp;
   sensorPacket["measurementCount"] = measurementCount_tmp;
   sensorPacket["packetCount"] = packetCount;
   sensorPacket["pumpVoltage"] = pumpAdcCount_tmp/4096.0f;
   sensorPacket["pumpEventState"] = (int)pumpToggleEvent_tmp;  // Cast to `int` so that the JSON lib doesn't interpret an enum as a bool
   sensorPacket["didTriggerPublish"] = didTriggerPublish_tmp;

   // Console spew
   Serial.print("\t\t");
   serializeJson(sensorPacket, Serial);
   Serial.println("");

   // Send the JSON object, and at the same time provide additional support data
   sendData(sensorPacket, payloadData, numberOfRetries);

   packetCount++;
}

void BilgeSensor::republishData(const payload_t &payloadData, uint32_t numberOfRetries)
{
   // Resend
   publishData(payloadData, numberOfRetries);

//   Serial.println(String("[Mesh] Resending: ") + String(lastTransmitTime) + String("/") + String("(") + String(cycleTime_old) + String(",") +
//                  String(cycleCount_old) + ") timed out after " + String(ellapsedTime) + " s, resending.");
}


/**
 * @brief BilgeSensor::setupPremesh Setup performed before the device connects to the mesh
 */
void BilgeSensor::setupPremesh()
{
   Particle.function("setTriggerTimeout_ms", &BilgeSensor::setTriggerTimeout, this);
   Particle.function("setTransmissionTimeout_ms", &BilgeSensor::setTransmissionTimeout, this);

}


/**
 * @brief BilgeSensor::setupPostmesh Setup performed after the device connects to the mesh
 */
void BilgeSensor::setupPostmesh()
{
   // Configure interrupts
   pinMode(BILGE_SENSOR_PIN, INPUT);

   // Attach the interrupt. Use a strange call to get around Arduino's documented issues with attaching
   // an interrupt to a class method. C.f. https://docs.particle.io/reference/device-os/firmware/xenon/#attachinterrupt-.
   // and https://community.particle.io/t/c-member-functions-as-function-pointers/21574
   attachInterrupt(BILGE_SENSOR_PIN, &BilgeSensor::bilgePumpCycleInterrupt, this, BILGE_SENSOR_INTERRUPT_MODE);

   // Configure ADC
   pinMode(PUMP_VOLTAGE_SENSOR_PIN, INPUT);
}
