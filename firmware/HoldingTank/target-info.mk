# Add this board to the list of buildable boards
ALL_BOARDS += holdingtank

# Set the board here that matches your Particle
# Should be one of: xenon,argon,boron
holdingtank_platform := xenon

holdingtank_serial_number := e00fce684d397b84a1cfd7ba
