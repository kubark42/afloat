#ifndef NODECONFIGURATION_H
#define NODECONFIGURATION_H

#include "HoldingTank.h"

#define NODE_TYPE HoldingTank

#define SENSOR_NAME "holding_tank"
#define DATA_FIELD_NAME "level"
#define DESCRIPTION "Monitors the holding tank"

#define SAMPLE_INTERVAL_MS (10/**60*/*1000)


#endif  // NODECONFIGURATION_H
