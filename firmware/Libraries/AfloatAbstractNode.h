#ifndef NODE_CLASS_H
#define NODE_CLASS_H

#include "Particle.h"

#define ARDUINOJSON_ENABLE_ARDUINO_STRING 1  // https://arduinojson.org/v6/faq/how-to-use-the-string-class-on-particle/
#include "Libraries/third_party/ArduinoJson-v6.13.0.h"  // https://arduinojson.org/

#include "payload_data_spec.h"


enum ACK_TUPLE_ENUM {
   TUPLE_ACKED,
   TUPLE_ACK_ID,
   TUPLE_LAST_XMIT_TIME,
   TUPLE_NUMBER_OF_RETRIES,
   TUPLE_PAYLOAD,
};

enum SLEEP_STATE {
   SLEEP_UNDETERMINED,
   SLEEP_DONT_SLEEP,
   SLEEP_TILL_INTERRUPTED,
   SLEEP_TILL_NEXT_SAMPLE,
   SLEEP_TILL_SYNC_TIME,
};


typedef std::tuple<bool      /*TUPLE_ACKED*/,
                   uint8_t   /*TUPLE_ACK_ID*/,
                   time_t    /*TUPLE_LAST_XMIT_TIME*/,
                   uint32_t  /*TUPLE_NUMBER_OF_RETRIES*/,
                   payload_t /*TUPLE_PAYLOAD*/> ackTuple_t;


class AfloatAbstractNode {
public:
   AfloatAbstractNode(String sensorName, String dataFieldName, String description, uint32_t sampleInterval_ms, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks);
   AfloatAbstractNode(String sensorName, String dataFieldName, String description, std::initializer_list<pin_t> wakeUpPins, std::initializer_list<InterruptMode> edgeTriggerModes, uint32_t connectionInterval_s, uint16_t maxNumPendingAcks);
   AfloatAbstractNode(const AfloatAbstractNode &obj);
   ~AfloatAbstractNode();

   void beginLoop(enum SLEEP_STATE *sleepState, bool *shouldSleepImmediately);
   void performNetworkHousekeeping(enum SLEEP_STATE *sleepState, bool *shouldDelaySleep);
   void handleSleep(SLEEP_STATE sleepState);
   void updateBatteryState();


protected:
   virtual void identityHandler(const char *event, const char *jsonMsgPacket) = 0;
   virtual void republishData(const payload_t &payloadData, uint32_t numberOfRetries = 0) = 0;

   virtual void setupPremesh() = 0;
   virtual void setupPostmesh() = 0;
   virtual enum SLEEP_STATE performMeasurement(enum SLEEP_STATE sleepState) = 0;

   void sendData(DynamicJsonDocument &sensorPacket, const payload_t &payloadData, const uint32_t numberOfRetries);

   void printAckTuple(ackTuple_t ackElement);
   void publishProvisioningMessage();
   void sleepUntil(time_t untilUtc_s);
   bool identityMsgProcessor(const char *event, const char *jsonMsgPacket);

   String sensorName;
   String dataFieldName;
   String description;
   uint32_t sampleInterval_ms;
   uint32_t connectionInterval_s;
   uint16_t maxNumPendingAcks;

   bool mustSendProvisioning = true;

   ackTuple_t *ackList;

private:
   std::initializer_list<pin_t> wakeUpPins;
   std::initializer_list<InterruptMode> edgeTriggerModes;

   const uint16_t ackTimeout_s = 5;  // Number of seconds to wait for an ACK
   const uint16_t maxNumberOfRetries = 3;  // Number of times to retry sending a packet before giving up on the mesh
};


#endif  // NODE_CLASS_H
