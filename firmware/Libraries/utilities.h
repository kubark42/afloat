#ifndef UTILITIES_H
#define UTILITIES_H

void delay_nonblocking(uint32_t delay_ms);
uint64_t getUtc_ms();

#endif  // UTILITIES_H
