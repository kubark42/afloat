 /* Project Afloat
  * Description: IOT network for sailboat
  * Author: Kenn Sebesta
  * Date: Jan 2020
  *
  * This class extends a basic circular buffer. Typically, data can only be removed from the ends
  * of a circular buffer, but in this class elements can be removed from arbitrary locations within
  * the buffer.
  *
  * The consequence of this random deletion is that on occasion the buffer can become fragmented. In
  * the event that the buffer runs out of space, it can try to compact itself to free unused space.
  *
  * It has the advantage of being firmly fixed in memory size, so that there is no accidental
  * memory leak or fragmentation.
  */


#ifndef CIRCULARBUFFERWITHFORGETTING_H
#define CIRCULARBUFFERWITHFORGETTING_H

#include <cstdlib>
#include <stdint.h>

template<typename T, size_t SIZE>
   class CircularBufferWithForgetting {

/***********************
 * METHOD DECLARATION
 ***********************/

   public:

      CircularBufferWithForgetting(){}
      ~CircularBufferWithForgetting(){}

      /**
       * @brief CircularBufferWithForgetting::enqueue Add new data to the end of the circular buffer
       * @param newVal Value to be added to the buffer
       */
      void enqueue(uint8_t newVal) {
         // Check if there's room. If not, try compacting the buffer to free up space.
         if (countStillActive == numBufferRows) {
            compactBuffer();
         }

         // Check again if there's room. If not, dequeue the oldest data.
         if (countStillActive == numBufferRows) {
            dequeue();
         }

         // Store data
         data[head] = newVal;

         // Increment head index
         head++;

         // Wrap around if necessary
         if (head >= numBufferRows) {
            head = 0;
         }

         // Increment the number of elements still active
         countStillActive++;
      }

      /**
       * @brief CircularBufferWithForgetting::remove Remove the ID from the list
       * @param ID
       */
      int32_t remove(uint8_t oldVal) {
         uint32_t tmpTail = head;

         // Loop through the entire circular buffer
         do {
            // Check if we've found the ID yet
            if (data[tmpTail] == oldVal) {
               // Clear the data
               data[tmpTail] = -1;

               // Decrement the count
               countStillActive--;

               // END
               return oldVal;
            }

            // Increment tmpTail
            tmpTail++;

            // Wrap tmpTail if necessary
            if (tmpTail >= numBufferRows) {
               tmpTail = 0;
            }
         } while(tmpTail != head);

      //   Serial.println("[ACK][WARNING] Item 0x" + String(ID, HEX) + " not found. head: " + String(head) + ", tail: " + String(head));

         return -1;
      }

   private:
      /**
       * @brief CircularBufferWithForgetting::compactBuffer Removes `-1` from the
       *        buffer, and shifts up the remaining data.
       */
      void compactBuffer() {
         // Get tail
         uint32_t tmpTail = (head-1 + numBufferRows) % numBufferRows;
         uint32_t tmpCount = 0;

         for (uint32_t i=0; i< numBufferRows; i++) {
            uint32_t idxOfInterest = (head - (i + 1) + numBufferRows) % numBufferRows;

            if (data[idxOfInterest] >= 0) {
               // In the beginning, the indices are the same. So only write once
               // they've diverged.
               if (tmpTail != idxOfInterest) {
                  data[tmpTail] = data[idxOfInterest];
               }

               // Wrap around if necessary. Be careful of the unsigned integer wrap-around where (0 - 1) --> 0xFFFF
               if (tmpTail == 0) {
                  tmpTail = numBufferRows-1;
               } else {
                  tmpTail--;
               }

               tmpCount++;
            }
         }

         /* Set instance variables */
         countStillActive = tmpCount;

         tail = tmpTail+1;
         // Wrap around if necessary
         if (tail >= 0) {
            tail = numBufferRows-1;
         }
      }


      /**
       * @brief CircularBufferWithForgetting::dequeue Removes the last element, as well
       *        as any other elements which are `-1`.
       */
      void dequeue() {
         // Loop across all deleted data
         do
         {
            // Increment tail index
            tail++;

            // Decrement the number of elements still active
            countStillActive--;

            // Wrap around if necessary
            if (tail >= numBufferRows) {
               tail = 0;
            }
         } while(data[tail] == -1 && countStillActive > 0);  // If the stored data is `-1`, then this represents a deleted data point
      }


/***********************
 * VARIABLE DECLARATION
 ***********************/
   public:
      uint32_t countStillActive;

   private:
      T data[SIZE];            // Contains the buffer data

      uint32_t head = 0;
      uint32_t tail = 0;

      const uint32_t numBufferRows = SIZE;
   };



#endif  // CIRCULARBUFFERWITHFORGETTING_H
