/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "utilities.h"


/**
 * @brief delay_nonblocking Delays without blocking Particle subsystem routines
 * @param delay_ms Time [in ms] to delay
 */
void delay_nonblocking(uint32_t delay_ms)
{
   uint32_t oldTime = millis();
   while (millis() - oldTime < delay_ms) {
      Particle.process();
   }
}


/**
 * @brief getUtc_ms Gets the UTC time in ms. Note, this does not handle wrap-around
 * of millis(), which overflows approx. once every 49 days.
 * @return UTC, in [ms]
 */
uint64_t getUtc_ms()
{
   // Get timestamp
   uint32_t tv_msec = millis();
   time_t tv_sec = Time.now();

   // Convert timestamp into UTC, in [ms]. This is a slow operation, do
   // it separately from capturing the timestamp
   uint64_t utc_ms= tv_sec * 1000ULL + (tv_msec % 1000);

   return utc_ms;
}

