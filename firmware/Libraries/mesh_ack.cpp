/*
 * Project Afloat
 * Description: IOT network for sailboat
 * Author: Kenn Sebesta
 * Date: Jan 2020
 */

#include "Particle.h"

#include "mesh_ack.h"
#include "CircularBufferWithForgetting.h"

//! Local macros

//! Local defines
#define BUFFER_DEPTH 10

//! Local variables
CircularBufferWithForgetting<int16_t, BUFFER_DEPTH> circBuf;
static uint8_t publishMsgCount = 0;


//! Local functions


/**
 * @brief publishWithAck Publish this message to the clout and request an ACK
 * @param name Message topic
 * @param packet Message payload
 * @param packetID Packet ID (used for tracking ACK)
 * @return Returns the result from Particle' Mesh.publish() command
 */
int32_t publishWithAck(const char *name, MeshPacketWithAck &packet, uint8_t *packetID)
{
   // Write the packet ID
   packet.packet->ID = publishMsgCount;

   // Publish the hybrid data object
   int32_t ret = Mesh.publish(name, (const char *) packet.packet);

   // On successful transmit (0), send this to the awaiting ack buffer
   if (ret == 0) {
      circBuf.enqueue(packet.packet->ID);
      publishMsgCount++;
   }

   if (packetID != nullptr) {
      *packetID = packet.packet->ID;
   }

   return ret;
}


/**
 * @brief receiveAck
 * @param ID The ID to remove from the buffer
 * @return ID if successful, -1 otherwise
 */
int32_t receiveAck(int32_t ackedID)
{
   int32_t ret = circBuf.remove(ackedID);

   /* Serial.println("Received ack! " + String(ID) + "/" + String(ret) + ", buffer filled: " + circBuf.countStillActive); */

   return ret;
}


/**
 * @brief haltAck Gives up on trying to ACK a particular message
 * @param ID to be removed
 * @return ID if the ID was still tracked, -1 otherwise
 */
int32_t haltAck(int32_t ID)
{
   int32_t ret = circBuf.remove(ID);

   // Console error message
   Serial.println("[MESH][WARNING] Canceling ack! " + String(ID) + "/" + String(ret) + ", buffer filled: " + circBuf.countStillActive);

   return ret;
}
