TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
      BilgeSensor/main.cpp \
      BilgeSensor/BilgeSensor.cpp \
      HoldingTank/main.cpp \
      HoldingTank/HoldingTank.cpp \
      Nexus/Nexus.cpp \
      Libraries/third_party/ArduinoJson-v6.13.0.h \
      Libraries/mesh_ack.cpp \
      Libraries/utilities.cpp \
      Libraries/AfloatAbstractNode.cpp \

HEADERS += \
      HoldingTank/HoldingTank.h \
      HoldingTank/payload_data_spec.h \
      HoldingTank/node_configuration.h \
      BilgeSensor/BilgeSensor.h \
      BilgeSensor/payload_data_spec.h \
      BilgeSensor/node_configuration.h \
      Libraries/gitcommit.template.h \
      Libraries/mesh_ack.h \
      Libraries/utilities.h \
      Libraries/AfloatAbstractNode.h \
      Libraries/CircularBufferWithForgetting.h \

OTHER_FILES += \
      BilgeSensor/gitcommit.h \
      Nexus/gitcommit.h \
      HoldingTank/gitcommit.h \
      BilgeSensor/target-info.mk \
      Nexus/target-info.mk \
      HoldingTank/target-info.mk \
      generate_git_header.py \
      Makefile \
      ../Makefile \
