from __future__ import absolute_import, division, generators, unicode_literals, print_function, nested_scopes, with_statement
import lxml.etree
import os, sys
import subprocess
import argparse
import platform


def findIotty(parentQuery):
    """
      Searches for the 'IOTTYDevice' string in a given child level, as defined by the parent query.
      Returns '' if nothing is found
      Returns the port name
    """

    # Loop an unreasonable bunch of times, but not for forever
    i = 0;
    while i < 1000:
        i = i + 1

        # Dive down to the ith child <key> element
        iottyDeviceQuery = parentQuery + '/child::key[' + str(i) + ']'
        childKey = doc.xpath(iottyDeviceQuery)

        try:
            # By simply trying to access this variable, we can test if it exists. If it
            # does, then we can test it. If the variable does not exist, then this
            # indicates we have reached the end of the list
            childKey[0].text

            # Check if we found the right field. This is the pList field attached to the port name.
            if (childKey[0].text == 'IOTTYDevice'):
               # Go forwards to the next `string` element
                portString = doc.xpath(iottyDeviceQuery + '/following-sibling::string')

                return portString[0].text
        except IndexError:
           # We found nothing, return an empty response
            return ''


def findUsbSerialNumber(parentQuery):
    """
      Searches for the 'USB Serial Number' string in a given child level, as defined by the parent query.
      Returns '' if nothing is found
      Returns the serial number
    """

    # Loop an unreasonable bunch of times, but not for forever
    i = 0;
    while i < 1000:
        i = i + 1

        # Dive down to the ith child <key> element
        usbSerialNumberQuery = parentQuery + '/child::key[' + str(i) + ']'
        childKey = doc.xpath(usbSerialNumberQuery)

        try:
           # By simply trying to access this variable, we can test if it exists. If it
           # does, then we can test it. If the variable does not exist, then this
           # indicates we have reached the end of the list
            childKey[0].text

            # Check if we found the right field. This is the pList field attached to the serial number.
            if (childKey[0].text == 'USB Serial Number'):
               # Go forwards to the next <string> element
                serialNumberString = doc.xpath(usbSerialNumberQuery + '/following-sibling::string')

                return serialNumberString[0].text
        except IndexError:
           # We found nothing, return an empty response
            return ''


def findDict(query, trackingString, func):
    """
      Recursively finds every chile <dict> tag. When a tag is called, the search function `func()` is called.
      Uses the `trackingString` variable to append the recursive depth. This has no effect but makes debugging easier.
      The function loops '' if the search function finds nothing.
      The function returns the result if the search function finds something
    """

    # Loop an unreasonable bunch of times, but not for forever
    i=0
    while i < 1000:
        i=i+1

        # Dive down to the ith child <dict> element
        dictQuery = query + '/child::dict[' + str(i) + ']'
        arrayKey = doc.xpath(dictQuery)

        try:
           # By simply trying to access this variable, we can test if it exists. If it
           # does, then we can test it. If the variable does not exist, then this
           # indicates we have reached the end of the list
            arrayKey[0].text

            # Go backwards to the prior <key> element
            arrayKeyQuery = doc.xpath(dictQuery + '/preceding-sibling::key')

            ret = findDict(dictQuery, trackingString + ", " + str(i), func)

            # If we have found a non-empty response, return it
            if ret != '':
                return ret

        except IndexError:
            ret = func(query)

            # If we have found a non-empty response, return it
            if ret != '':
                return ret

             # We found nothing, return an empty response
            return ''


def get_port_name(serialNumber):
    """
      Searches for the port name associated with a given USB serial number.
    """

    # Generate the XPath query string
    serialNumberQuery = '//string[text()="' + serialNumber + '"]'

    # Perform the query. Note that this likely returns several elements. We will choose between them later.
    serialNumberElementList = doc.xpath(serialNumberQuery)

    # Sanity check the query result
    if serialNumberElementList == []:
        print("Serial number `"+ serialNumber + "` not found")
        quit()

    # Iterate over all the instances of that serial number
    for testIdx in range(len(serialNumberElementList)):
        # Go up a level to the parent in order to start at the top of the children
        baseQuery = '(' + serialNumberQuery + ')[' + str(testIdx+1) + ']/parent::dict'
        portName = findDict(baseQuery, '0', findIotty)

        # If we have found the modem port, return it
        if portName != '':
            return portName


def get_serial_number(modemPort):
    """
      Searches for the USB serial number associated with a given port name.
    """

    # Generate the XPath query string
    modemQueryString = '//string[text()="' + modemPort + '"]'

    # Perform the query. Note that this likely returns several elements. We will choose between them later.
    modemPortElementList = doc.xpath(modemQueryString)

    # Sanity check the query result
    if modemPortElementList == []:
        print("Modem port `"+ modemPort + "` not found")
        quit()

    # Iterate over all the instances of that port
    for testIdx in range(len(modemPortElementList)):
        baseQuery = '(' + modemQueryString + ')[' + str(testIdx+1) + ']/ancestor::dict[4]'
        serialNumber = findDict(baseQuery, '0', findUsbSerialNumber)

        # If we have found the serial number, return it
        if serialNumber != '':
            return serialNumber
            break


if __name__ == "__main__":
    # Check if this is macOS
    if platform.system() != 'Darwin':
        print('Only macOS is supported')
        quit()

    # Read all the arguments
    parser = argparse.ArgumentParser(description='Fills a header template with git information')
    parser.add_argument( '--serial_number', action = 'store', type = str, default = None, help = 'Optional USB serial number')
    parser.add_argument( '--port', action = 'store', type = str, default = None, help = 'Optional modem port')
    parser.add_argument( '--file', action = 'store', type = str, default = None, help = 'Optional plist file')

    # Parse the arguments
    args = parser.parse_args()

    # Check that we exactly one of these two is set
    if len([x for x in (args.port, args.serial_number) if x is not None]) != 1:
       parser.error('You must give exactly one of --serial_number or --port')

    # Get shorthhand variables for the parsed input
    plistFile = args.file
    inputSerialNumber = args.serial_number
    inputModemPort = args.port

    if plistFile is None:
        # Get USB plist. Calls the command `ioreg -a -r -l -t -k "USB Serial Number" -k "IODialinDevice"`. This command has the magical property
        # of grouping all the USB serial port names and USB serial numbers.
        usbPlist_xml = subprocess.check_output(['ioreg', '-a', '-r', '-l', '-t', '-k', 'USB Serial Number', '-k', 'IODialinDevice'])

        # Parse plist XML data
        doc = lxml.etree.fromstring(usbPlist_xml)
    else:
        # Parse plist XML data
        doc = lxml.etree.parse(plistFile)

    if inputModemPort is not None:
        serialNumber = get_serial_number(inputModemPort)
        print(serialNumber)

    if inputSerialNumber is not None:
         portName = get_port_name(inputSerialNumber)
         print(portName)
