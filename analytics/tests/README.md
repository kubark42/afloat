# Analytics tests

This directory contains the analytics testing code. For the moment, it's all built on pytest (https://docs.pytest.org/en/latest/) and hypothesis (https://hypothesis.readthedocs.io/en/latest/).

Code has been tested with Anaconda Python 3.7.6 on Debian Buster x86_64

## Requirements

* `pytest` (https://docs.pytest.org/en/latest/)
* `hypothesis` (https://hypothesis.readthedocs.io/en/latest/)

## Running tests

pytest recursively searches for files of the name `test_*.py` and runs the test code in those files. For more info on pytest recursion and source file naming, see [https://docs.pytest.org/en/latest/goodpractices.html#conventions-for-python-test-discovery]

* To run the suite of python tests: `pytest`
* To run the suite of python tests and create artificial errors for a positive control: `pytest --fail`
* To run an individual test: `pytest test_foo.py`
* To run tests and enter pdb upon error: `pytest --pdb`


