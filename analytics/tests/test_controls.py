# -*- coding: utf-8 -*-
# pytest positive & negative controls

# No error (negative control) is default
# To induce errors, use either:
#   pytest --fail
#   python test_controls.py --fail

import pytest as pt 
import sys

######################################################################
# following tests all pass
######################################################################

def test_simple_pass():
    assert 1 + 2 == 3

def test_approx_scalar_pass():
    t = 1/3.0
    y = 123.0 * t
    assert 123.0 == pt.approx(y / t)


######################################################################
# following tests all FAIL. Run with: pytest --fail
######################################################################

def test_simple_fail(fail):
    if fail:
        assert 1 + 2 == 4
    else:
        pass

def test_approx_scalar_fail(fail):
    if fail:
        t = 1/3.0
        y = 123.0 * t
        assert 321.0 == pt.approx(y / t)
    else:
        pass

if __name__ == '__main__':
    pt.main(sys.argv)
